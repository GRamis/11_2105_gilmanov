/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package task23;

/**
 *
 * @author �����
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

class ConnectionFactory {

    private final String driverClassName = "org.postgresql.Driver";
    private final String connectionUrl = "jdbc:postgresql://localhost:5432/postgres";
    private final String dbUser = "postgres";
    private final String dbPwd = "postgres";
    private static ConnectionFactory connectionFactory = null;

    private ConnectionFactory() {
        try {
            Class.forName(driverClassName);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public Connection getConnection() throws SQLException {
        Connection conn = null;
        conn = DriverManager.getConnection(connectionUrl, dbUser, dbPwd);
        return conn;
    }

    public static ConnectionFactory getInstance() {
        if (connectionFactory == null) {
            connectionFactory = new ConnectionFactory();
        }
        return connectionFactory;
    }
}

public class Task23 {

    Connection con = null;
    PreparedStatement ptmt = null;
    ResultSet rs = null;

    private Connection getConnection() throws SQLException {
        return ConnectionFactory.getInstance().getConnection();
    }

    public void zapros() throws SQLException {
        try {
            con.setAutoCommit(false);
            String str = "UPDATE book SET price = price + price*0.05";
            con = getConnection();
            ptmt = con.prepareStatement(str);
            rs = ptmt.executeQuery();
            con.commit();
        } catch (Exception e) {
            con.rollback();
        } finally {
            if (con != null) {
                con.close();
            }
        }
    }
}
