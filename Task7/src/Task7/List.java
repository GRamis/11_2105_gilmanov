package Task7;

import java.io.PrintWriter;
import java.util.Comparator;

public class List  {
        
    private Node first;//поле первого элемента типа Node
    protected int size;//размер
    
    public final int size(){
        return size;
    }

   /*
    * класс Node содержит значение элемента и ссылка на следующий элемент
    */
    private class Node {

        private Object value; //значение элемента
        private Node next; //сылка на следующий элемент
            /*
             * конструктор 
             */
        public Node(Node next, Object value) {
            this.next = next;
            this.value = value;
        }
    }
    
    /**
     * метод добавления первого элемента со ссылкой на следующий элемент
     */
    public void add(Object element) {
        size++;
        first = new Node(first, element);
    }
    
    public void printList(List list, PrintWriter file){
        Node q = first;
            for (int i = 0; i<10; i++){
            file.println(q.value + " ");
            q = q.next;
        }
    }
    /*
     * метод сортировки
     */
    public void sort() {
        first = mergeSort(first);
    }
    
    public Node mergeSort(Node head) {
        if (head == null || head.next == null) {
            return head;
        }
        Node a = head;
        Node b = head.next;
        while (b != null && b.next != null) {
            a = a.next;
            b = b.next.next;
        }
        b = a.next;
        a.next = null;
        return merge(mergeSort(head), mergeSort(b));
    }
    
        void print() {
        Node q = first;
        for (int i = 0; i<10; i++){
            System.out.println(q.value + " ");
            q = q.next;
        }
    }
    
    public Node merge(final Node a, final Node b) {
        Node a1 = a;
        Node b1 = b;
        Node c = null;
        Node head = null;
        while (a1 != null && b1 != null) {
            if (((Comparable) a1.value).compareTo(b1.value) > 0) {
                if (c != null) {
                    c.next = a1;
                    c = a1;
                } else {
                    c = a1;
                    head = c;
                }                
                a1 = a1.next;
            } else {
                if (c != null) {
                    c.next = b1;
                    c = b1;
                } else {
                    c = b1;
                    head = c;
                }
                b1 = b1.next;
            }
        }
        if (a1 == null) {
            if (c == null) {
                c = b1;
                head = c;
            } else {
                c.next = b1;
            }
        }
        if (b1 == null) {
            if (c == null) {
                c = a1;
                head = c;
            } else {
                c.next = a1;
            }
        }
        return head;
    }
    }