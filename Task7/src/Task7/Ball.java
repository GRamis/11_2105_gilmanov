package Task7;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 *
 * @author Рамис
 */
public class Ball {

    public static void main(String[] args) throws FileNotFoundException  {
        List a = new List();
       Scanner b = new Scanner(new File("input.txt"));
        try (PrintWriter n = new PrintWriter(new File("output.txt"))) {
            while(b.hasNextLine()){
                a.add(new Balls(b.next(),b.nextInt()));
            }
            a.sort();
            a.print();
            a.printList(a, n);
        }
    }
} 