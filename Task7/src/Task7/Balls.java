package Task7;

/**
 *
 * @author Рамис
 */
public class Balls implements Comparable<Balls> {
    private String name;
    private Integer points;

    public Balls(String name, Integer points) {
        this.name = name;
        this.points = points;
    }

    public Balls() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }
    
    @Override
    public String toString(){
        return getName() + "" + getPoints();
    }
    
    @Override
    public int compareTo(Balls o) {
        return this.getPoints().compareTo(o.getPoints());
    }
}
