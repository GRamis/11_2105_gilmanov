package task48;

import java.net.*;
import java.io.*;

public class EchoServer {

	public void initServer() throws IOException {
		ServerSocket serverSocket = null;
		String[] arr = { "0-Info 1-Aries 2-Taurus 3-Gemini 4-Cancer 5-Leo 6-Virgo 7-Libra 8-Scorpio 9-Sagittarius 10-Capricorn 11-Aquarius 12-Pisces", 
				"Aries 21.03 - 20.04", 
				"Taurus 21.04 - 20.05", 
				"Gemini 21.05 - 21.06", 
				"Cancer 22.06 - 22.07",
				"Leo 23.07 - 22.08", 
				"Virgo 23.08 - 23.09", 
				"Libra 24.09 - 23.10", 
				"Scorpio 24.10 - 22.11", 
				"Sagittarius 23.11 - 21.12", 
				"Capricorn 22.12 - 20.01", 
				"Aquarius 21.01 - 18.02",
				"Pisces 19.02 - 20.03" };
		try {
			serverSocket = new ServerSocket(12345);
		} catch (IOException e) {
			System.err.println("Could not listen on port: 12345.");
			System.exit(1);
		}
		Socket clientSocket = null;
		System.out.println("Waiting for connection.....");

		try {
			clientSocket = serverSocket.accept();
		} catch (IOException e) {
			System.err.println("Accept failed.");
			System.exit(1);
		}

		System.out.println("Connection successful");
		System.out.println("Waiting for input.....");

		PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
		BufferedReader in = new BufferedReader(new InputStreamReader(
				clientSocket.getInputStream()));

		int inputInt;
		String inputLine;

		while ((inputLine = in.readLine()) != null) {
			inputInt = Integer.parseInt(inputLine);
			System.out.println("Server: " + inputLine);
			if(inputInt >= 0 && inputInt <= 12) {
				inputLine = arr[inputInt];
			}
			else {
				inputLine = "Input 0 for Info";
			}
			out.println(inputLine);
		}
		out.close();
		in.close();
		clientSocket.close();
		serverSocket.close();
	}

	public static void main(String[] args) throws IOException {
		EchoServer echoServer = new EchoServer();
		echoServer.initServer();
	}
}