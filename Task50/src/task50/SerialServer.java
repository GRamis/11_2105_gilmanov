/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package task50;

import java.net.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.io.*;

public class SerialServer {
    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = null;

        try {
            serverSocket = new ServerSocket(10007);
        } catch (IOException e) {
            System.err.println("Could not listen on port: 10007.");
            System.exit(1);
        }

        Socket clientSocket = null;

        try {
            System.out.println("Waiting for Client");
            clientSocket = serverSocket.accept();
        } catch (IOException e) {
            System.err.println("Accept failed.");
            System.exit(1);
        }

        ObjectOutputStream out = new ObjectOutputStream(clientSocket.getOutputStream());
        ObjectInputStream in = new ObjectInputStream(clientSocket.getInputStream());

       Student stud;

        try {
        	stud = (Student) in.readObject();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

		Path path = Paths.get("D://1.jpg");
		byte[] data = Files.readAllBytes(path);

        stud = new Student("Gilmanov", "Ramis", 2105, data);
        System.out.println("Server sending point: " + stud + " to Client");
        out.writeObject(stud);
        out.flush();

        out.close();
        in.close();
        clientSocket.close();
        serverSocket.close();
    }
} 