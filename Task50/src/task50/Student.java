/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package task50;

/**
 *
 * @author Рамис
 */
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;

public class Student implements Serializable{
    public String name;
    public String surname;
    public int group;
    private transient byte[] photo;
    private transient Date data;

    public Student (String name, String surname, int group, byte [] image){
        this.name = name;
        this.surname = surname;
        this.group = group;
        this.image = image;

    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getGroup() {
        return group;
    }

    public void setGroup(int group) {
        this.group = group;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    private void readObject(
            ObjectInputStream aInputStream
    ) throws ClassNotFoundException, IOException {
        aInputStream.defaultReadObject();
        Date date1 = new Date();
        date = date1;
    }
    private void writeObject(
            ObjectOutputStream aOutputStream
    ) throws IOException {
        aOutputStream.defaultWriteObject();
    }
}


