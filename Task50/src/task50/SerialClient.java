package task50;

import java.io.*;
import java.net.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class SerialClient {
	public static void main(String[] args) throws IOException {

		Socket echoSocket = null;
		ObjectOutputStream out = null;
		ObjectInputStream in = null;

		try {
			echoSocket = new Socket("127.0.0.1", 10007);

			out = new ObjectOutputStream(echoSocket.getOutputStream());
			in = new ObjectInputStream(echoSocket.getInputStream());

		} catch (UnknownHostException e) {
			System.err.println("Don't know about host: taranis.");
			System.exit(1);
		} catch (IOException e) {
			System.err.println("Couldn't get I/O for "
					+ "the connection to: taranis.");
			System.exit(1);
		}
		/* Send and read Point3d */
		Student stud = null;
		Path path = Paths.get("C://foto.jpg");
		byte[] data = Files.readAllBytes(path);

		System.out.println("Sending point: " + stud + " to Server");
		out.writeObject(stud);
		out.flush();
		System.out.println("Send point, waiting for return value");

		try {
			stud = (Student) in.readObject();
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		out.close();
		in.close();
		echoSocket.close();
	}
}