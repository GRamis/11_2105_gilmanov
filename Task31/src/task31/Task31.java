package task31;

/**
 *
 * @author Рамис
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;

public class Task31 {
        public static void main(String[] args) throws Exception {
    Thread task1 = new Thread(new Task("in1.txt"));
    task1.start();
    Thread task2 = new Thread(new Task("in2.txt"));
    task2.start();
    Thread task3 = new Thread(new Task("in3.txt"));
    task3.start();
}
}

class Task implements Runnable{

    String fileName;

    public Task(String fileName) {
        this.fileName = fileName;
    }   
    
    @Override
    public void run() {
        try {
            task(fileName);
        } catch(Exception e){
            e.printStackTrace();
        }
    }
    
    void task(String f) throws FileNotFoundException, IOException{
        File file = new File(f);
        FileReader fr = new FileReader(file);
        LineNumberReader lnr = new LineNumberReader(fr);
        String line = "";
        int count = 0;
        while ((line = lnr.readLine()) != null) {
          //System.out.println("Line Number " + lnr.getLineNumber() + ": " + line);
          count++;
        }
        System.out.println("Line Number " + count);
        fr.close();
        lnr.close();    
    }
}