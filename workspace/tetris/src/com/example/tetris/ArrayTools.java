package com.example.tetris;

public class ArrayTools {

	public static boolean IsInArray(int value, int[] array)
	{
		for (int i = 0; i < array.length; i++) {
			if(value == array[i])
				return true;
		}
		return false;
	}
}