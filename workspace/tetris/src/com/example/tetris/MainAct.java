package com.example.tetris;

import com.example.tetris.tetris.TetrisView;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class MainAct extends Activity {
	
	TetrisView gV;
	GenericMenu gM;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        gV = new TetrisView(this);
        setContentView(gV);
    }
    
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
    	gV.setGameFocus(hasFocus);
    	super.onWindowFocusChanged(hasFocus);
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	gM = new GenericMenu(menu);
    	gM.populate();
    	return super.onCreateOptionsMenu(menu);
    }
    
    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
    	if(item.getTitle().equals("Restart"))
    		gV.restartGame();
    	if(item.getTitle().equals("Quit"))
    		finish();
    	return super.onMenuItemSelected(featureId, item);
    }

}