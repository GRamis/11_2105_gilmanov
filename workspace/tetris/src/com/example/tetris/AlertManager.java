package com.example.tetris;

import com.example.tetris.tetris.TetrisView;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

public class AlertManager {

	public static final int TYPE_GAME_OVER = 0;
	public static final int GAME_OVER_RESTART = DialogInterface.BUTTON1;
	public static final int GAME_OVER_QUIT = DialogInterface.BUTTON2;
	
	private static boolean alertActive = false;

	public static boolean IsAlertActive()
	{
		return alertActive ;
	}
	
	public static void PushAlert(TetrisView v) {
		int title, msg, ok, cancel;
		OnClickListener l;
				title = R.string.GameOver;
				msg = R.string.Replay;
				ok = R.string.Restart;
				cancel = R.string.Quit;
				l = getGameOverClickListener(v);
		
		alertActive = true;

		AlertDialog a = new AlertDialog.Builder(v.getContext())
	      .setInverseBackgroundForced(true)
	      .setCancelable(false)
	      .setTitle(title)
	      .setMessage(msg)
	      .setNegativeButton(cancel, l)
	      .setPositiveButton(ok, l)
	      .create();
		
		a.show();
	}	

	public static void Resolve() {
		alertActive = false;
	}    
	
	private static OnClickListener getGameOverClickListener(final TetrisView v){
		return new  DialogInterface.OnClickListener(){
			public void onClick(DialogInterface dialog, int which) {
				switch(which)
				{
					case GAME_OVER_RESTART:
						v.restartGame();
						break;
					case GAME_OVER_QUIT:
						v.quitGame();
						break;
				}
				AlertManager.Resolve();
			}
		};
	}
}