package com.example.tetris.tetris;

import com.example.tetris.AlertManager;

import android.app.Activity;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.KeyEvent;
import android.view.View;

public class TetrisView extends View implements ITetrisConstants {
	
	private boolean mHasFocus; 
	public  void setGameFocus( boolean hasFocus ) {	mHasFocus = hasFocus;	}
	private long mNextUpdate; 	
	private long mLastGravity;	
	private int mTicks;			
	private int mSeconds;		
    private Paint mPaint;	
    private Activity mActivityHandle;
  
    
    private TetrisGrid grid; 		
    private TetrisShape currentShape;	
    private int currentAction;		
    
    public TetrisView(Activity context) {

		super(context);
		mActivityHandle = context;
		setBackgroundColor(Color.BLACK);
		setFocusable(true);
		setFocusableInTouchMode(true);
		requestFocus();
		
		grid = new TetrisGrid();
		currentShape = new TetrisShape(grid);
		mPaint = new Paint();
		
		init();
	}
	

	private void init() {

		currentShape.isInited = false;
		currentAction = ACTION_NONE;
		mNextUpdate = 0;
		mTicks = 0;
		mSeconds = 0;
		mLastGravity = 1;

		grid.init();
		
	}

	public void restartGame() {
		init();
		currentShape.isGameOver = false;
	}


	public void quitGame() {
		mActivityHandle.finish();
	}
	
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		grid.setBackGroundDimentions(w, h);
		super.onSizeChanged(w, h, oldw, oldh);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if(DISREGARD_MULTIPLE_KEYPRESSED && event.getRepeatCount() < 1)
		{
			switch(keyCode)
			{
				case KeyEvent.KEYCODE_DPAD_LEFT:
				case KeyEvent.KEYCODE_4:
				{
					currentAction = ACTION_STRAFE_LEFT;
					break;
				}
				case KeyEvent.KEYCODE_DPAD_RIGHT:
				case KeyEvent.KEYCODE_6:
				{
					currentAction = ACTION_STRAFE_RIGHT;
					break;
				}
				case KeyEvent.KEYCODE_DPAD_UP:
				case KeyEvent.KEYCODE_2:
				{
					currentAction = ACTION_ROTATE_L;
					break;
				}
				case KeyEvent.KEYCODE_DPAD_DOWN:
				case KeyEvent.KEYCODE_8:
				{
					currentAction = ACTION_ROTATE_R;
					break;
				}
				case KeyEvent.KEYCODE_5:
				case KeyEvent.KEYCODE_ENTER:
				case KeyEvent.KEYCODE_SPACE:
				case KeyEvent.KEYCODE_DPAD_CENTER:
				{
					currentAction = ACTION_MAKE_FALL;
					break;
				}
			}
		}
		return super.onKeyDown(keyCode, event);
	}

	public void update() {
		long time = System.currentTimeMillis();

		
		if( mHasFocus )
		{
			if(currentShape.isGameOver)
			{
				if(!AlertManager.IsAlertActive())
				{
					AlertManager.PushAlert(this);
				}

			}
			else if( time > mNextUpdate )
			{
				mNextUpdate = time + 1000 / FRAME_RATE;
				mTicks++;
				currentShape.update(currentAction);
				currentAction = ACTION_NONE;
				if(time - mLastGravity > GRAVITY_RATE || currentShape.IsFalling())
				{
					mLastGravity = time;
					boolean shapeIsLocked = currentShape.addGravity();
					if(shapeIsLocked)
					{
						int points = grid.update();
						if(points != 0){
					}
				}
				
				if(mTicks/FRAME_RATE > mSeconds)
				{
					mSeconds = mTicks/FRAME_RATE;
				}
			}
		}
		else
		{
			mNextUpdate = time + (1000 / OUT_OF_PAUSE_DELAY);
		}
		return;}
	}

	@Override
	protected void onDraw(Canvas canvas) {
		
		update();
		
		super.onDraw(canvas);
		grid.paint(canvas, mPaint);
		invalidate();
	}
}