package com.example.tetris.tetris;

import android.graphics.Color;


public interface ITetrisConstants {

	public static final int FRAME_RATE = 48;
	public final static int OUT_OF_PAUSE_DELAY = FRAME_RATE*2;
	public final static boolean DISREGARD_MULTIPLE_KEYPRESSED = true;

	public static final int ACTION_NONE = 0;
	public static final int ACTION_STRAFE_LEFT = 1;
	public static final int ACTION_STRAFE_RIGHT = 2;
	public static final int ACTION_ROTATE_L = 3;
	public static final int ACTION_ROTATE_R = 4;
	public static final int ACTION_MAKE_FALL = 5;
	
	public static final boolean PLAYFIELD_USE_MARGINS = true;
	public static final int MARGIN_TOP = 36;
	public static final int MARGIN_LEFT = 12;
	public static final int MARGIN_RIGHT = 64;
	public static final int MARGIN_BOTTOM = 36;
	public static final int PLAYFIELD_COLS = 12;
	public static final int PLAYFIELD_ROWS = 18;
	
	public static final int GRAVITY_RATE = (1000 / 2);

	public static final int START_CELL		= (PLAYFIELD_COLS/2)-1;
	public static final int C_CENTER = 0;
	public static final int C_LEFT = -1;
	public static final int C_UP = -PLAYFIELD_COLS;
	public static final int C_RIGHT = 1;
	public static final int C_DOWN = PLAYFIELD_COLS;

	public static final int DONT_CHECK_CELL = -1000;

	public static final int STATE_USER    = 0;
	public static final int STATE_LOCKED  = 1;
	public static final int STATE_FALLING = 2;
	
	public static final int ROT_LEFT  = -1;
	public static final int ROT_RIGHT =  1;

	public static final int OR_NORTH = 0;
	public static final int OR_EAST  = 1;
	public static final int OR_SOUTH = 2;
	public static final int OR_WEST  = 3;
	public static final int START_ORIENTATION = OR_EAST;
	
	public static final int ELEM_BASE = 0;
	public static final int ELEM_1 	  = 1;
	public static final int ELEM_2    = 2;
	public static final int ELEM_3    = 3;
	public static final int MAX_ELEMS = 4;
	public static final int TYPE_LONG 		= 0;
	public static final int TYPE_BL   		= 1;
	public static final int TYPE_L 	  		= 2;
	public static final int TYPE_SQUARE 	= 3;
	public static final int TYPE_S 			= 4;
	public static final int TYPE_BS 		= 5;
	public static final int TYPE_T 			= 6;
	public static final int TYPE_MAX_TYPES	= 7;
	
	public static final int SHAPE_TABLE_ELEMS_1 	  = 0;
	public static final int SHAPE_TABLE_ELEMS_2 	  = 1;
	public static final int SHAPE_TABLE_ELEMS_3 	  = 2;
	public static final int SHAPE_TABLE_ELEMS_PER_ROW = 3;
	public static final int SHAPE_TABLE_ROWS_PER_TYPE = 4;
	public static final int SHAPE_TABLE_TYPE_OFFSET	  = SHAPE_TABLE_ROWS_PER_TYPE*SHAPE_TABLE_ELEMS_PER_ROW;
	
	public static final int[] SHAPE_TABLE =
	{
		C_UP, C_DOWN, C_UP*2, //I
		C_LEFT, C_RIGHT, C_RIGHT*2, //---
		C_UP, C_DOWN, C_DOWN*2, //I
		C_LEFT, C_RIGHT, C_LEFT*2,//---
		
		C_UP, C_DOWN, C_UP+C_RIGHT,//backwards L
		C_LEFT, C_RIGHT, C_RIGHT+C_DOWN,//--|
		C_UP, C_DOWN, C_DOWN+C_LEFT,//inverse L
		C_LEFT, C_RIGHT, C_LEFT+C_UP,//|--

		C_UP, C_DOWN, C_UP+C_LEFT,//L
		C_LEFT, C_RIGHT, C_RIGHT+C_UP,//--|
		C_UP, C_DOWN, C_DOWN+C_RIGHT,//L
		C_LEFT, C_RIGHT, C_LEFT+C_DOWN,//|--
		
		C_RIGHT, C_DOWN, C_RIGHT+C_DOWN,//O
		C_RIGHT, C_DOWN, C_RIGHT+C_DOWN,//O
		C_RIGHT, C_DOWN, C_RIGHT+C_DOWN,//O
		C_RIGHT, C_DOWN, C_RIGHT+C_DOWN,//O

		C_DOWN+C_LEFT, C_DOWN, C_RIGHT,//_|-
		C_UP, C_RIGHT, C_RIGHT+C_DOWN,//|-i
		C_DOWN+C_LEFT, C_DOWN, C_RIGHT,//_|-
		C_UP, C_RIGHT, C_RIGHT+C_DOWN,//|-i

		C_LEFT, C_DOWN, C_DOWN+C_RIGHT,//-|_
		C_UP+C_RIGHT, C_RIGHT, C_DOWN,//i-|
		C_LEFT, C_DOWN, C_DOWN+C_RIGHT,//-|_
		C_UP+C_RIGHT, C_RIGHT, C_DOWN,//i-|
		
		C_UP, C_DOWN, C_RIGHT,//T
		C_LEFT, C_DOWN, C_RIGHT,//T
		C_LEFT, C_DOWN, C_UP,//T
		C_LEFT, C_UP, C_RIGHT,//T
	};
	
}