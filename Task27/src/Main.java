import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {

   static PreparedStatement pst = null;
   static ResultSet rs = null;
   static Connection con = null;

    static void pst1() throws SQLException{
        pst = con.prepareStatement("SELECT name FROM employee WHERE job_title IN (SELECT job_title FROM employee WHERE name = 'Виктор')");
        rs = pst.executeQuery();
        while(rs.next()){
            System.out.println(rs.getString(1));
        }
    }
    static void pst2() throws SQLException{
        pst = con.prepareStatement("SELECT name FROM employee WHERE job_title IN (SELECT a.job_title FROM employee a, dept b WHERE a.name = 'Виктор' AND a.dept_no = b.id)");
        rs = pst.executeQuery();
        while(rs.next()){
            System.out.println(rs.getString(1));
        }
    }
    static void pst3() throws SQLException{
        pst = con.prepareStatement("SELECT * FROM employee WHERE salary = (SELECT MIN(salary) FROM employee)");
        rs = pst.executeQuery();
        while(rs.next()){
            System.out.println(rs.getString(2));
        }
    }
    static void pst4() throws SQLException{
        pst = con.prepareStatement("SELECT name FROM employee WHERE job_title IN (SELECT a.job_title FROM employee a, dept b WHERE b.name = 'sales')");
        rs = pst.executeQuery();
        while (rs.next()){
            System.out.println(rs.getString(1));
        }
    }
    static void pst5() throws SQLException{
        pst = con.prepareStatement("SELECT name FROM employee WHERE job_title IN (SELECT a.job_title FROM employee a, dept b WHERE a.job_title = 'Аналитик')");
        rs = pst.executeQuery();
        while (rs.next()){
            System.out.println(rs.getString(1));
        }
    }
    //AVG
    static void pst6() throws SQLException{
        pst = con.prepareStatement("SELECT b.name FROM employee b WHERE b.salary > (SELECT AVG(a.salary) FROM employee a)");
        rs = pst.executeQuery();
        while (rs.next()){
            System.out.println(rs.getString(1));
        }
    }
    static void pst7() throws SQLException{
        pst = con.prepareStatement("SELECT d.name, COUNT(e.job_title) FROM employee e, dept d WHERE e.job_title = 'Аналитик' AND e.dept_no = d.id" + " GROUP BY d.name, d.id");
        rs = pst.executeQuery();
        while (rs.next()){
            System.out.println(rs.getString(1));
        }
    }
    static void pst8() throws SQLException{
        pst = con.prepareStatement("SELECT d.name, SUM(e.salary) FROM employee e, dept d WHERE e.dept_no = d.id" + " GROUP BY d.name, d.id HAVING COUNT(e.dept_no)>2");
        rs = pst.executeQuery();
        while (rs.next()){
            System.out.println(rs.getString(1));
        }
    }

    public static void main(String[] args) {

        String url = "jdbc:postgresql://localhost:5432/postgres";
        String user = "postgres";
        String password = "postgres";

        try {
            con = DriverManager.getConnection(url, user, password);
            pst8();

        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(Main.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);

        } finally {

            try {
                if (pst != null) {
                    pst.close();
                }
                if (con != null) {
                    con.close();
                }

            } catch (SQLException ex) {
                Logger lgr = Logger.getLogger(Main.class.getName());
                lgr.log(Level.SEVERE, ex.getMessage(), ex);
            }
        }
    }
}