package task33;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 *
 * @author Рамис
 */

public class CallableTicket implements Callable <Integer>{
    Integer start;
    Integer end;

    public CallableTicket(Integer start, Integer end) {
        this.start = start;
        this.end = end;
    }
    
    @Override
    public Integer call() throws Exception {
        Integer count=0;
        for (Integer i=start; i <end; i++){
            if (isHappy(i))           {
                count ++;
            }
        }
        return count;
    }
    static boolean isHappy(Integer a){
        int arr [] = new int [6];
        int k;
        for (int i=0;i<6;i++){
            k = a%10;
            arr[i] = k;
            a = a/10;
        }
        int s1 = arr[0]+arr[1]+arr[2];
        int s2 = arr[3]+arr[4]+arr[5];
        if (s1 == s2)  {
            return true;
        }

        return false;
    }

public static void main (String args[]) throws ExecutionException, InterruptedException {
    CallableTicket ex1 = new CallableTicket(0, 333334);
    CallableTicket ex2 = new CallableTicket(333334, 666667);
    CallableTicket ex3 = new CallableTicket(666667, 1000000);

    ExecutorService executor = Executors.newFixedThreadPool(3);

    Future<Integer> f1 = executor.submit(ex1);
    Future<Integer> f2 = executor.submit(ex2);
    Future<Integer> f3 = executor.submit(ex3);

    System.out.println("Number ="+ (f1.get() + f2.get()+ f3.get()));

}
}

/*
 * MyCallable implements Callable<Srting>
 * String call(){
 * }
 * return str;
 * }
 * 
 * ft = new FutureTask(myCallable);
 * new Tread(ft).start();
 * str = ft.get();
 */


/*
 * Executor Service
 * Executors.newFixedThreadPool(10); // 10 потоков, в которых он выполняет задачки =)
 * for(int i = 0; i<100; i++){
 * es.submit(myRunnable);
 * }
 * es = Executors.newCahedThreadPool();
 * es = Executors.newScheduledThreadScheduledExecutor();
 * es.schedule(myRunnable,5 ,TimeUnit.SECONDS);
 * es.schedule(AtFixedRate(myRunnable,0,1,TU.SECONDS);
 * es.scheduleWithFixedDelay(-----------"-----------);
 */