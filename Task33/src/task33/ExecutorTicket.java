package task33;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicReference;

public class ExecutorTicket {
    
    static boolean isHappy(Integer a){
        int arr [] = new int [6];
        int k;
        for (int i=0;i<6;i++){
            k = a%10;
            arr[i] = k;
            a = a/10;
        }
        int s1 = arr[0]+arr[1]+arr[2];
        int s2 = arr[3]+arr[4]+arr[5];
        if (s1 == s2)  {
            return true;
        }

        return false;
    }
    private final static AtomicReference<Integer> Atomic1 = new AtomicReference<>();
    private final static AtomicReference<Integer> Atomic2 = new AtomicReference<>();
    private final static AtomicReference<Integer> Atomic3 = new AtomicReference<>();

    public static void main(String[] args) throws ExecutionException, InterruptedException {

        Thread t1 = new Thread( new Runnable(){
            int count =0;
            public void run(){
               for (int i=0; i <333334; i++){
                   if (isHappy(i)){
                       count ++;
                   }
               }
                Atomic1.set(count);
            }

        });
    Thread t2 = new Thread( new Runnable(){
        int count =0;
        public void run(){
            for (int i=333334; i <666667; i++){
                if (isHappy(i)){
                    count ++;
                }
            }
            Atomic2.set(count);
            }
    });
    Thread t3 = new Thread( new Runnable() {
        int count =0;
        public void run(){
            for (int i=666667; i <1000000; i++){
                if (isHappy(i)){
                    count ++;
                }
            }
            Atomic3.set(count);
            }
    });

        t1.start();
        t2.start();
        t3.start();
        t1.join();
        t2.join();
        t3.join();
        System.out.println("Number= "+ (Atomic1.get()+ Atomic2.get()+ Atomic3.get()));
}
}