/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package task38;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;


public class ProducerConsumerService {

    public static void main(String[] args) {
        //Creating BlockingQueue of size 10
        BlockingQueue<Message> queue = new ArrayBlockingQueue<>(10);
        Producer producer = new Producer(queue);
        Consumer consumer = new Consumer(queue);
                System.out.println("Producer and Consumer has been started");

        for (int i = 0; i < 5; i++) {
        //starting producer to produce messages in queue
        new Thread(producer).start();
        //starting consumer to consume messages from queue
        new Thread(consumer).start();
        }
        System.out.println("Producer and Consumer started");
    }
}