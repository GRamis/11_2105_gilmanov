package task36;

class Debet {
	static int count = 100;
	
	public static synchronized int getDebet() {
		return count;
	}

    public static boolean setCount(int count) {
        return Debet.count > count;
    }
        
    public static synchronized void setDebet(int count) {
        if(setCount(count) == true){
		Debet.count -= count;
	}}
}
class Money implements Runnable {
	int outmoney;
	
	public Money(int outmoney) {
		this.outmoney = outmoney;
	}
	public void run() {
		Debet.setDebet(Debet.getDebet() - outmoney);
		System.out.println(Debet.getDebet());
	}
}
public class Task36 {
	public static void main(String[] args) {
		
		Money d1 = new Money(30);
		Thread t1 = new Thread(d1);
		t1.start();
		
		Money d2 = new Money(40);
		Thread t2 = new Thread(d2);
		t2.start();
	}
}