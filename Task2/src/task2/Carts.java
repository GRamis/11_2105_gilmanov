/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package task2;

/**
 *
 * @author Рамис
 */
public class Carts {
    private Names name;
    private Suits suit;

    public Carts(Names name, Suits suit) {
        this.name = name;
        this.suit = suit;
    }

    public Names getName() {
        return name;
    }

    public void setName(Names name) {
        this.name = name;
    }

    public Suits getSuit() {
        return suit;
    }

    public void setSuit(Suits suit) {
        this.suit = suit;
    }
    
    @Override
    public String toString(){
        return name + " " + suit;
    }

}