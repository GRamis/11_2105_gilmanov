package task2;

import java.util.Arrays;
import java.util.Random;

public class Deck {
    private Carts[] cards = new Carts[36];
    
    public Deck(){
        for(int i = 0; i<36; i++){
            cards[i]=new Carts(Names.values()[i/9], Suits.values()[i/9]);
        }
    }
    
    public Carts[] get() {
            return cards;
    }
        
    public void mix(Carts[]cards){
        Random r = new Random();
        for(int i = 0; i<36; i++){
            int cur = r.nextInt(36);
            Carts current = cards[i];
            cards[i] = cards[cur];
            cards[cur] = current;
        }
    }
    
    @Override
    public String toString(){
        return Arrays.toString(cards);
    }
}