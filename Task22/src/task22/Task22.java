/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package task22;

/**
 *
 * @author Рамис
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

class ConnectionFactory {

    private final String driverClassName = "org.postgresql.Driver";
    private final String connectionUrl = "jdbc:postgresql://localhost:5432/postgres";
    private final String dbUser = "postgres";
    private final String dbPwd = "postgres";
    private static ConnectionFactory connectionFactory = null;

    private ConnectionFactory() {
        try {
            Class.forName(driverClassName);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public Connection getConnection() throws SQLException {
        Connection conn = null;
        conn = DriverManager.getConnection(connectionUrl, dbUser, dbPwd);
        return conn;
    }

    public static ConnectionFactory getInstance() {
        if (connectionFactory == null) {
            connectionFactory = new ConnectionFactory();
        }
        return connectionFactory;
    }
}

public class Task22 {

    Connection con = null;
    PreparedStatement ptmt = null;
    ResultSet rs = null;

    private Connection getConnection() throws SQLException {
        return ConnectionFactory.getInstance().getConnection();
    }

    public void t1() {
        try {
            String str = "SELECT name FROM author WHERE name LIKE 'N%' or name LIKE 'S%' ";
            con = getConnection();
            ptmt = con.prepareStatement(str);
            rs = ptmt.executeQuery();
            while (rs.next()) {
                System.out.println(rs.getString(1));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ptmt != null) {
                    ptmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void t2() {
        try {
            String str = "SELECT name FROM book WHERE price>1000 ";
            con = getConnection();
            ptmt = con.prepareStatement(str);
            rs = ptmt.executeQuery();
            while (rs.next()) {
                System.out.println(rs.getString(1));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ptmt != null) {
                    ptmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void t3() {
        try {
            String str = "SELECT book.name, author.name FROM book, author WHERE author.id = book.author_id";
            con = getConnection();
            ptmt = con.prepareStatement(str);
            rs = ptmt.executeQuery();
            while (rs.next()) {
                System.out.println(rs.getString(1) + " " + rs.getString(2));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ptmt != null) {
                    ptmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void t4() {
        try {
            String str = "SELECT name FROM book WHERE price IN (SELECT MIN(price) FROM book)";
            con = getConnection();
            ptmt = con.prepareStatement(str);
            rs = ptmt.executeQuery();
            while (rs.next()) {
                System.out.println(rs.getString(1));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ptmt != null) {
                    ptmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void t5() {
        try {
            String str = "SELECT AVG(price) FROM book";
            con = getConnection();
            ptmt = con.prepareStatement(str);
            rs = ptmt.executeQuery();
            while (rs.next()) {
                System.out.println(rs.getString(1));// getInt()
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ptmt != null) {
                    ptmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void t6() {
        try {
            String str = "SELECT COUNT(id) FROM author";
            con = getConnection();
            ptmt = con.prepareStatement(str);
            rs = ptmt.executeQuery();
            while (rs.next()) {
                System.out.println(rs.getString(1));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ptmt != null) {
                    ptmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void t7() {
        try {
            String str = "SELECT author.name,MAX(book.price)FROM author,book GROUP BY author.name";
            con = getConnection();
            ptmt = con.prepareStatement(str);
            rs = ptmt.executeQuery();
            while (rs.next()) {
                System.out.println(rs.getString(1) + " " + rs.getString(2));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ptmt != null) {
                    ptmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void t8() {
        try {
            String str = "SELECT COUNT(id) as amount,title FROM books GROUP BY title";
            con = getConnection();
            ptmt = con.prepareStatement(str);
            rs = ptmt.executeQuery();
            while (rs.next()) {
                System.out.println(rs.getString(1) + " " + rs.getString(2));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ptmt != null) {
                    ptmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void t9() {
        try {
            String str = "SELECT COUNT(name) FROM authors GROUP BY name HAVING COUNT(name)>1";
            con = getConnection();
            ptmt = con.prepareStatement(str);
            rs = ptmt.executeQuery();
            while (rs.next()) {
                System.out.println(rs.getString(1) + " " + rs.getString(2));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ptmt != null) {
                    ptmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void t10() {
        try {
            String str = "SELECT a.name, AVG(b.price) FROM authors a, books b GROUP BY b.author_id,a.name HAVING AVG(price)>1000";
            con = getConnection();
            ptmt = con.prepareStatement(str);
            rs = ptmt.executeQuery();
            while (rs.next()) {
                System.out.println(rs.getString(1) + " " + rs.getString(2));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ptmt != null) {
                    ptmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void t11() {
        try {
            String str = "SELECT a.name FROM authors a, books b WHERE a.id=b.author_id AND b.price = (SELECT MAX(price) FROM books)";
            con = getConnection();
            ptmt = con.prepareStatement(str);
            rs = ptmt.executeQuery();
            while (rs.next()) {
                System.out.println(rs.getString(1) + " " + rs.getString(2));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ptmt != null) {
                    ptmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void t12() {
        try {
            String str = "SELECT a.name FROM authors a,books b WHERE a.id=b.author_id GROUP BY b.author_id,a.name HAVING COUNT(*)>1";
            con = getConnection();
            ptmt = con.prepareStatement(str);
            rs = ptmt.executeQuery();
            while (rs.next()) {
                System.out.println(rs.getString(1) + " " + rs.getString(2));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ptmt != null) {
                    ptmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}