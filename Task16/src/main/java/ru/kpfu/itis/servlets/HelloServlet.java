package ru.kpfu.itis.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class HelloServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    public void doGet (HttpServletRequest request,
                       HttpServletResponse response) throws ServletException, IOException {

            String numb1 = request.getParameter("numb1");
            String numb2 = request.getParameter("numb2");
        int a = Integer.parseInt(numb1);
        int b = Integer.parseInt(numb2);
            request.setAttribute("message","Sum = "+ a+b);
            getServletConfig().getServletContext().getRequestDispatcher(
                    "/WEB-INF/jsp/hello.jsp").forward(request, response);

    }
}
