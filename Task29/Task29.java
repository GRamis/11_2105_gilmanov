class Task implements Runnable {

    private static String getName;
    String name;

    public Task() {
    }

    public Task(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public void run() { //конструктор, getName()
        for (int i = 0; i < 20; i++) {
            System.out.println(name +" lap: " + i);
        }
    }
}

public class Task29 {
    public static void main(String[] args) {
        Task MR = null;
        for (int i = 0; i < 5; i++) {
            MR = new Task("CAR" + i);
            Thread t = new Thread(MR, MR.getName());
            t.start();
        }
    }
}