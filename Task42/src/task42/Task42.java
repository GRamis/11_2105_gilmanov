/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package task42;

import java.util.concurrent.CountDownLatch;

public class Task42 {
	public static void main(String[] args) throws InterruptedException {
		final CountDownLatch startGate = new CountDownLatch(10);
		Thread th1 = new Thread() {
                    @Override
			public void run() {
				try {
					startGate.await();
					System.out.println("Концерт начался");
				} catch (InterruptedException e) {
				}
			}
		};
		th1.start();
		for (int i = 0; i < 30; i++) {
			Thread th2 = new Thread("Зритель") {
                            @Override
				public void run() {
					System.out.printf("пришел %s%n", getName());
					startGate.countDown();

				}
			};
			th2.start();
		}

	}
}