package ru.kpfu.itis.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

public class HelloServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    HashMap<String, String> map = new HashMap<>();

    public void init(){
    map.put("Ramis", "12qw");
    }

    @Override
    public void doPost (HttpServletRequest request,
                       HttpServletResponse response) throws ServletException, IOException {
        int a = 1;

            String login = request.getParameter("login");
            String password = request.getParameter("password");

        if(map.get(login)!=null) {
        if(map.get(login).equals(password)){
             request.setAttribute("message", a);
             getServletConfig().getServletContext().getRequestDispatcher(
                     "/site/firstApp.jsp").forward(request, response);
         }else{
            a = -1;
            request.setAttribute("message", a);
            getServletConfig().getServletContext().getRequestDispatcher("/index.jsp").forward(request,response);
        }
        } else{
            a = -1;
            request.setAttribute("message", a);
            getServletConfig().getServletContext().getRequestDispatcher("/index.jsp").forward(request,response);
        }
}
}