<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<!-- Все хорошо, за исключением того, что если размер аватарки будет чуть меньше, то описание профиля поплывет, а нам надо чтобы оно было полностью справа. 
Почитай про верстку на div'ах и css свойство float.
http://htmlbook.ru/samlayout/verstka-na-html5/maket-saita
http://htmlbook.ru/samlayout/verstka-na-html5/vnutrennyaya-stranitsa
-->
<meta charset="UTF-8"/>
<html>

<head>

    <link rel="stylesheet" href="ava.css">

    <title>Studentbook ITIS</title>

</head>

<body>

<h1>Studentbook ITIS</h1>
<img src="book.jpg" width="180" height="100" class="ava" >
<ul>
    <li>Гильманов Рамис</li>
    <li>Дата рождения : 26.07.1994</li>
    <li>Группа: 11-2105</li>
    <li>Лаборатория : Unknown</li>
    <li>Активности : пассивен</li>
    <li>Дополнительная информация : При наличии гугла богоподобен)</li>
    <a href="http://vk.com/gilmanov_ramis">http://vk.com/gilmanov_ramis</a>
</ul>

</form>

</body>

</html>
