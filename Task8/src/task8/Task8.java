/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package task8;

/**
 *
 * @author Рамис
 */
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Task8 {
	
	static ArrayList<String> stud = new ArrayList<>(); 

    public static void read() throws FileNotFoundException{
    	
    	    Scanner sc = new Scanner(new File("input.txt"));
	    String Student = sc.nextLine();
	        
	    while (sc.hasNextLine()){
	    	stud.add(Student);
	        Student = sc.nextLine();
	    }  
	    Collections.sort(stud);
    }	
    
    public static void write() throws IOException{
        try (FileWriter fw = new FileWriter(new File("output.txt"))) {
            for(int i = 0; i < stud.size(); i++) {
            fw.write(stud.get(i) + "\r\n");
            fw.flush();
            }
        }
    }
    
    public static void main (String args[]) throws IOException {
        read();
        write();
    }
}
