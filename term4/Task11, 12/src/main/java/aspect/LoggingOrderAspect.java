package aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;

/**
 * Created by Рамис on 19.03.14.
 */
@Aspect
public class LoggingOrderAspect {

    @Before("execution(* service.OrderService.*(..))")
    public void logBeforeGet(JoinPoint joinpoint){
        System.out.println("Order logBefore is running");
        System.out.println("hi" + joinpoint.getSignature().getName());
        System.out.println("******");
    }

    @AfterThrowing(
            pointcut = "execution(* service.OrderService.getOrder(..))",
            throwing= "error")
    public void logAfterTrowing(JoinPoint joinPoint, Throwable error){
        System.out.println("getOrder() is running!");
        System.out.println("hijacked : " + joinPoint.getSignature().getName());
        System.out.println("Exception : " + error);
        System.out.println("******");
    }

    @Around(
            "execution(* service.OrderService.addOrder(..))")
    public void logAround(ProceedingJoinPoint joinPoint) throws Throwable{
        long start = System.nanoTime();
        System.out.println("addOrder() is running!");
        joinPoint.proceed();

        System.out.println("addOrder before is running!");
        joinPoint.proceed();
        System.out.println("addOrder after is running!");
        long end = System.nanoTime();
        long traceTime = end-start;
        System.out.println("Time: "+traceTime);

        System.out.println("******");

    }
}