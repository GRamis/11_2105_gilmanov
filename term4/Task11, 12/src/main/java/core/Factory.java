package core;

import service.decorator.CustomerDec;
import service.decorator.OrderDec;
import service.impl.CustomerServiceImpl;
import service.impl.OrderServiceImpl;

public class Factory {

    private static Factory factory = new Factory();

    private Factory() {}

    public static Factory getInstance() {
        return factory;
    }

    public Object getBean(String name) {
        if(name.equals("CustomerService")) {
            return new CustomerDec(new CustomerServiceImpl()) {
            };
        }
        if(name.equals("OrderService")) {
            return new OrderDec(new OrderServiceImpl()) {
            };
        }

        throw new IllegalArgumentException();
    }  }