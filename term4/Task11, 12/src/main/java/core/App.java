package core;

import service.OrderService;

public class App {
	public static void main(String[] args) throws Exception {

		//ApplicationContext appContext = new ClassPathXmlApplicationContext("Spring-Customer.xml");

        Factory factory = Factory.getInstance();

      //  CustomerService customer = (CustomerService) factory.getBean("CustomerService");
//		customer.addCustomer();
		
		//customer.addCustomerReturnValue();
		
		//customer.addCustomerAround("customer#1");

  //      customer.addCustomerThrowException();

       OrderService order = (OrderService) factory.getBean("OrderService");

        order.addOrder("24", Long.MIN_VALUE);

        order.getOrder("24");

	}
}