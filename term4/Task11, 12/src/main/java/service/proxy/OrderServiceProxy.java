package service.proxy;

import service.OrderService;

/**
 * Created by Рамис on 25.03.14.
 */
public class OrderServiceProxy implements OrderService {

    private OrderService target;

    public OrderServiceProxy(OrderService orderService){
        target = orderService;
    }

    @Override
    public String getOrder(String orderNumber) {
        System.out.println("getOrder is running");
        return target.getOrder(orderNumber);
    }

    @Override
    public String addOrder(String orderNumber, Long amount) {
        System.out.println("logBefore() is running!");
        return target.addOrder(orderNumber, amount);
     }
}
