package service.decorator;

import service.OrderService;

/**
 * Created by Рамис on 26.03.14.
 */
class Order extends OrderDec{

    protected Order(OrderService c) {
        super(c);
    }

    public String getOrder(String orderNumber) {
        System.out.println("getOrder is running");
        return component.getOrder(orderNumber + " WER WER");
    }

    public String addOrder(String orderNumber, Long amount) {
        System.out.println("logBefore() is running!");
        return component.addOrder(orderNumber + " HELLO :) ", amount);
    }
}
