package service.decorator;

import service.OrderService;

/**
 * Created by Рамис on 26.03.14.
 */
public abstract class OrderDec implements OrderService {
    protected OrderService component;

    protected OrderDec(OrderService c) {
        component = c;
    }

    public String getOrder(String orderNumber) {
        System.out.println("getOrder is running");
        return component.getOrder(orderNumber);
    }

    public String addOrder(String orderNumber, Long amount) {
        System.out.println("logBefore() is running!");
        return component.addOrder(orderNumber, amount);
    }
}

