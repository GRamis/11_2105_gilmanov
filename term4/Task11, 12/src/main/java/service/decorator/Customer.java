package service.decorator;

/**
 * Created by Рамис on 26.03.14.
 */
class Customer extends CustomerDec{

    public Customer(CustomerDec c) {
        super(c);
    }

    public void addCustomer() {
        System.out.println("logBefore() is running!");
        System.out.println("Run");
        component.addCustomer();
    }
}
