package service.decorator;

import service.CustomerService;

/**
 * Created by Рамис on 26.03.14.
 */
public abstract class CustomerDec implements CustomerService {
    protected CustomerService component;

    public CustomerDec(CustomerService c){
        component = c;
    }

    public void addCustomer() {
        System.out.println("logBefore() is running!");
        component.addCustomer();
    }

    public String addCustomerReturnValue() {
        return null;
    }

    public void addCustomerThrowException() throws Exception {

    }

    public void addCustomerAround(String name) {

    }
}

