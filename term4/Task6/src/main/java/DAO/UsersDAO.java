package DAO;

import java.sql.SQLException;
import java.util.List;
import logic.Users;


public interface UsersDAO {
    public void addUsers(Users student) throws SQLException;   //добавить пользователя
    public void updateUsers(Users student) throws SQLException;//обновить пользователя
    public Users getUsersById(Long id) throws SQLException;    //получить пользователя по id
    public List getAllUsers() throws SQLException;              //получить всех пользователей
    public void deleteUsers(Users student) throws SQLException;//удалить пользователя
}

