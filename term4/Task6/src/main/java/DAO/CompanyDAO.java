package DAO;

import logic.Company;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Рамис on 01.03.14.
 */
public interface CompanyDAO{
    public void addCompany(Company company) throws SQLException;   //добавить компанию
    public void updateCompany(Company company) throws SQLException;//обновить компанию
    public Company getCompanyById(Long id) throws SQLException;    //получить компанию по id
    public List getAllCompany() throws SQLException;              //получить все компании
    public void deleteCompany(Company company) throws SQLException;//удалить компанию
}
