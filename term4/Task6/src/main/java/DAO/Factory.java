package DAO;

import DAO.Impl.*;


public class Factory {
      
      private static UsersDAO usersDAO = null;
      private static VisitDAO visitDAO = null;
    private static ResumeDAO resumeDAO = null;
    private static Res_CategDAO res_categDAO = null;
    private static JobsDAO jobsDAO = null;
    private static CompanyDAO companyDAO = null;
    private static CategoryDAO categoryDAO = null;
      private static Factory instance = null;

      public static synchronized Factory getInstance(){
            if (instance == null){
              instance = new Factory();
            }
            return instance;
      }

      public UsersDAO getUsersDAO(){
            if (usersDAO == null){
              usersDAO = new UsersDAOImpl();
            }
            return usersDAO;
      }

    public static VisitDAO getVisitDAO() {
        if (visitDAO == null){
            visitDAO = new VisitDAOImpl();
        }
        return visitDAO;
    }

    public static ResumeDAO getResumeDAO() {
        if(resumeDAO == null){
            resumeDAO = new ResumeDAOImpl();
        }
        return resumeDAO;
    }

    public static Res_CategDAO getRes_categDAO() {
        if(res_categDAO == null){
            res_categDAO = new Res_CategImpl();
        }
        return res_categDAO;
    }

    public static JobsDAO getJobsDAO() {
        if(jobsDAO == null){
            jobsDAO = new JobsDAOImpl();
        }
        return jobsDAO;
    }

    public static CompanyDAO getCompanyDAO() {
        if(companyDAO == null){
            companyDAO = new CompanyDAOImpl();
        }
        return companyDAO;
    }

    public static CategoryDAO getCategoryDAO() {
        if(categoryDAO == null){
            categoryDAO = new CategoryDAOImpl();
        }
        return categoryDAO;
    }
}