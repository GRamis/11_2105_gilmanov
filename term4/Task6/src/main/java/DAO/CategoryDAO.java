package DAO;

import logic.Category;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Рамис on 01.03.14.
 */
public interface CategoryDAO{
    public void addCategory(Category category) throws SQLException;   //добавить категорию
    public void updateCategory(Category category) throws SQLException;//обновить категорию
    public Category getCategoryById(Long id) throws SQLException;    //получить категорию по id
    public List getAllCategory() throws SQLException;              //получить все категории
    public void deleteCategory(Category category) throws SQLException;//удалить категорию
}
