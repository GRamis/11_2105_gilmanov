package DAO;

import logic.Users;
import logic.Visit;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Рамис on 01.03.14.
 */
public interface VisitDAO{
    public void addVisit(Visit visit) throws SQLException;   //добавить приглашение
    public void updateVisit(Visit visit) throws SQLException;//обновить приглашение
    public Visit getVisitById(Long id) throws SQLException;    //получить приглашение по id
    public List getAllVisit() throws SQLException;              //получить список приглашений
    public void deleteVisit(Visit visit) throws SQLException;//удалить приглашение
}
