package logic;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * Created by Рамис on 26.02.14.
 */
@Entity
@Table(name="Jobs")
public class Jobs {

    int id_job;
    Company id_company;
    int id_categ;
    String text;
    String title;
    String experience;
    String salary;

    Jobs(){
    }

    Jobs(String title) {
        this.title = title;
    }

    @Id
    @GeneratedValue(generator="increment")
    @GenericGenerator(name="increment", strategy = "increment")
    @Column(name="id_job")
    public int getId_job() {
        return id_job;
    }

    public void setId_job(int id_job) {
        this.id_job = id_job;
    }

    @ManyToOne
    @JoinTable(name="id_company")
    public Company getId_company() {
        return id_company;
    }

    public void setId_company(Company id_company) {
        this.id_company = id_company;
    }

    @Column(name="id_categ")
    public int getId_categ() {
        return id_categ;
    }

    public void setId_categ(int id_categ) {
        this.id_categ = id_categ;
    }

    @Column(name="text")
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Column(name = "title")
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Column(name = "experience")
    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    @Column(name = "salary")
    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }
}

