package logic;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * Created by Рамис on 26.02.14.
 */
@Entity
@Table(name="Visit")
public class Visit {

    int id_job;
    int id_resume;
    String type;

    @Id
    @GeneratedValue(generator="increment")
    @GenericGenerator(name="increment", strategy = "increment")
    @Column(name="id_job")
    public int getId_job() {
        return id_job;
    }

    public void setId_job(int id_job) {
        this.id_job = id_job;
    }
    @Column(name="id_resume")
    public int getId_resume() {
        return id_resume;
    }

    public void setId_resume(int id_resume) {
        this.id_resume = id_resume;
    }
    @Column(name="type")
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}