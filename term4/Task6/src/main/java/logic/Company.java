package logic;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Рамис on 26.02.14.
 */
@Entity
@Table(name="Company")
public class Company {

    int id_company;
    String name_company;
    String town;
    String email;
    String passw;
    List<Jobs> job;

    @OneToMany
    @JoinTable(name = "jobs")
    public List<Jobs> getJob() {
        return job;
    }

    public void setJob(List<Jobs> job) {
        this.job = job;
    }

    @Id
    @GeneratedValue(generator="increment")
    @GenericGenerator(name="increment", strategy = "increment")
    @Column(name="id_company")
    public int getId_company() {
        return id_company;
    }

    public void setId_company(int id_company) {
        this.id_company = id_company;
    }
    @Column(name="name_company")
    public String getName_company() {
        return name_company;
    }

    public void setName_company(String name_company) {
        this.name_company = name_company;
    }
    @Column(name="town")
    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }
    @Column(name="email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    @Column(name="passw")
    public String getPassw() {
        return passw;
    }

    public void setPassw(String passw) {
        this.passw = passw;
    }
}
