package foo.bar;

public class Zoo {

    Animal animal;

    public Zoo(Animal animal) {
        this.animal = animal;
    }

    public void zooCity(String city) {
        if (animal != null) {
            System.out.println("Zoo " + city);
        }
    }
}
