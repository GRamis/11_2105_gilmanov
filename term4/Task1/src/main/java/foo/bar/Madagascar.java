package foo.bar;

import java.util.List;

public class Madagascar {

    private Animal animal;

    private List<Penguins> penguinses;

    public void setPenguinses(List<Penguins> penguinses) {
        this.penguinses = penguinses;
    }

    public void setAnimal(Animal animal){
        this.animal = animal;
    }

    public void setCity(String city) {
        if (animal != null) {
            System.out.println("The penguin " + penguinses.get(2).getPgname() + " from " + city);
        }
    }
}
