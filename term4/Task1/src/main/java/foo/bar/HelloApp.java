package foo.bar;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class HelloApp {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        Animal animal = (Animal)context.getBean("animalAlex");
        Zoo zoo = context.getBean(Zoo.class);
        zoo.zooCity("New York");
        System.out.println(animal.getName());

        Madagascar madagascar = context.getBean(Madagascar.class);
        madagascar.setCity("Madagascar");
    }
}
