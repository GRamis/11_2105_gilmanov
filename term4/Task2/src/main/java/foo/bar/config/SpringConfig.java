package foo.bar.config;

import foo.bar.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ComponentScan;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Рамис on 14.02.14.
 */

@Configuration
@ComponentScan("foo.bar")
public class SpringConfig {
     @Bean
    public Animal animalAlex(){
        return new Animal("Alex");
    }

    @Bean
    public Animal animalMelman(){
        return new Animal("Melman");
    }

    @Bean
    public Animal animalMarty(){
        return new Animal("Marty");
    }

    @Bean
    public Penguins penguinSkipper(){
        Penguins penguins = new Penguins();
        penguins.setPgname("Skipper");
        return penguins;
    }

    @Bean
    public Penguins penguinsKowalski(){
        Penguins penguins = new Penguins();
        penguins.setPgname("Kowalski");
        return penguins;
    }

    @Bean
    public Penguins penguinsRiko(){
        Penguins penguins = new Penguins();
        penguins.setPgname("Riko");
        return penguins;
    }

    @Bean
    public Penguins penguinsSoldier(){
        Penguins penguins = new Penguins();
        penguins.setPgname("Soldier");
        return penguins;
    }

    @Bean
    public Madagascar madagascar(){
        Madagascar madagascar = new Madagascar();
        madagascar.setAnimal(animalAlex());
        List<Penguins> penguins = new ArrayList();
        penguins.add(penguinSkipper());
        penguins.add(penguinsKowalski());
        penguins.add(penguinsRiko());
        penguins.add(penguinsSoldier());
        madagascar.setPenguinses(penguins);
        return madagascar;

    }
}
