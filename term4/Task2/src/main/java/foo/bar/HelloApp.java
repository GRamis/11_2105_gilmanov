package foo.bar;

import foo.bar.config.SpringConfig;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class HelloApp {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext();
        context.register(SpringConfig.class);
        context.refresh();
        Animal animal = (Animal)context.getBean("animalAlex");
        Zoo zoo = context.getBean(Zoo.class);
        zoo.zooCity("New York");
        System.out.println(animal.getName());

        Madagascar madagascar = context.getBean(Madagascar.class);
        madagascar.setCity("Madagascar");
    }
}