package foo.bar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class Zoo {

    Animal animal;
    @Autowired
    public Zoo(@Qualifier("animalAlex") Animal animal) {
        this.animal = animal;
    }

    public void zooCity(String city) {
        if (animal != null) {
            System.out.println("Zoo " + city);
        }
    }
}
