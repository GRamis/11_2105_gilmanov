package foo.bar;

/**
 * Created by Рамис on 14.02.14.
 */
import org.springframework.stereotype.Component;

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface Cat {
    String value() default "";
}
