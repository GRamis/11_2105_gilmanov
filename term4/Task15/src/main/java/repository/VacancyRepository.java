package repository;

import model.CV;
import model.Vacancy;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface VacancyRepository extends CrudRepository<Vacancy, Long> {
    List<Vacancy> findByTitle(String title);

    @Modifying
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Query("update Vacancy vacancy set vacancy.title = ?2 where vacancy.title LIKE ?1")
    int setNewTitle(String title, String newTitle);

    @Transactional
    @Query("select vacancy from Vacancy vacancy join vacancy.category c where c.id = ?1")
        //      SELECT * FROM vacancy, vacancy_category WHERE ... AND vacancy_category.category_id=?1
    Iterable<Vacancy> findByCategory(Long categoryID);

    List<Vacancy> findByTitleStartingWithIgnoreCase(String title);
}
