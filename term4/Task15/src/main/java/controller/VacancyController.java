package controller;

import controller.editor.CategoryEditor;
import model.Category;
import model.Vacancy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import service.CompanyService;
import service.SearchService;
import viewobject.CVVO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Рамис on 06.05.14.
 */

@Controller
public class VacancyController{

private CompanyService companyService;
private SearchService searchService;
    @Autowired

    public VacancyController(CompanyService companyService, SearchService searchService) {
        this.companyService = companyService;
        this.searchService = searchService;
    }

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Category.class, new CategoryEditor());
    }
       @RequestMapping("/vacancy/list")
        public ModelAndView getVacancy(){
            ModelAndView mv = new ModelAndView("vacancy_list");
            mv.addObject("vacancyList", companyService.getAllVacancy());
            mv.addObject("category", 0);
            mv.addObject("allCategories", searchService.getAllCategories());
            return mv;
        }

    @RequestMapping("/vacancy/list/{categoryId}")
    public ModelAndView getCVByCategory(@PathVariable Long categoryId) {
        ModelAndView mv = new ModelAndView("vacancy_list");
        mv.addObject("vacancyList", companyService.getVacancyListByCategoryId(categoryId));
        mv.addObject("category", categoryId);
        mv.addObject("allCategories", searchService.getAllCategories());
        return mv;
    }

    @RequestMapping("/vacancy/edit/{id}")
    public ModelAndView editVacancy(@PathVariable Long id) {
        ModelAndView mv = new ModelAndView("vacancy_edit");
        mv.addObject("vacancy", companyService.getVacancyByIdWithCategories(id));
        mv.addObject("allCategories", searchService.getAllCategories());
        return mv;
    }

    @RequestMapping("/vacancy/create")
    public ModelAndView editVacancy() {
        ModelAndView mv = new ModelAndView("vacancy_edit");
        mv.addObject("vacancy", new Vacancy());
        mv.addObject("allCategories", searchService.getAllCategories());
        return mv;
    }

    @RequestMapping("/vacancy/save")
    public String saveVacancy(Vacancy vacancy) {
        companyService.saveVacancy(vacancy);
        return "redirect:/vacancy/list";
    }

    @RequestMapping("/vacancy/search")
    public @ResponseBody
    List<CVVO> getVacancy(@RequestParam String term) {
        Iterable<Vacancy> vacancies = companyService.getVacancyByNamePart(term);
        List<CVVO> result = new ArrayList<CVVO>();
        for (Vacancy vacancy:vacancies) {
            result.add(new CVVO(vacancy.getId(), vacancy.getTitle()));
        }
        return result;
    }

}
