package service.impl;

import model.Vacancy;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.CompanyRepository;
import repository.VacancyRepository;
import service.CompanyService;

@Service
public class CompanyServiceImpl implements CompanyService{

    private CompanyRepository companyRepository;
    private VacancyRepository vacancyRepository;

    @Autowired
    public CompanyServiceImpl(CompanyRepository companyRepository, VacancyRepository vacancyRepository) {
        this.companyRepository = companyRepository;
        this.vacancyRepository = vacancyRepository;
    }

    @Override
    public Vacancy getVacancyById(Long id) {
        return vacancyRepository.findOne(id);
    }

    @Override
    public Vacancy getVacancyByIdWithCategories(Long id) {
        Vacancy vacancy = vacancyRepository.findOne(id);
        Hibernate.initialize(vacancy.getCategory());
        return vacancy;
    }

    @Override
    public Iterable<Vacancy> getAllVacancy() {
        return vacancyRepository.findAll();
    }

    @Override
    public Iterable<Vacancy> getVacancyByNamePart(String term) {
        return vacancyRepository.findByTitleStartingWithIgnoreCase(term);
    }

    @Override
    public void saveVacancy(Vacancy vacancy) {
         vacancyRepository.save(vacancy);
    }

    @Override
    public Vacancy getVacancyListByCategoryId(Long categoryID) {
        return vacancyRepository.findOne(categoryID);
    }
}
