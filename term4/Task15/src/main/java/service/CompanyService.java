package service;

import model.Company;
import model.Vacancy;

public interface CompanyService {
    Vacancy getVacancyById(Long id);

    Vacancy getVacancyByIdWithCategories(Long id);

    Iterable<Vacancy> getAllVacancy();

    public Iterable<Vacancy> getVacancyByNamePart(String term);

    void saveVacancy(Vacancy vacancy);

    Vacancy getVacancyListByCategoryId(Long categoryID);
}
