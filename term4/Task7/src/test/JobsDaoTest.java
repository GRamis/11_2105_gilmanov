import DAO.Factory;
import DAO.JobsDAO;
import logic.Jobs;
import org.junit.Test;

import java.sql.SQLException;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Рамис on 14.03.14.
 */
public class JobsDaoTest {
    private JobsDAO JobsDAO = Factory.getInstance().getJobsDAO();

    private Jobs createJobs(){
        Jobs jobs = new Jobs();
        jobs.setText("Java");
        jobs.setTitle("Software developer");
        jobs.setExperience("5");
        jobs.setSalary("100000");
        return jobs;
    }

    @Test
    public void testFindAll() throws SQLException {
        List<Jobs> companyJobs = JobsDAO.getAllJobs();
        assertNotNull(companyJobs);
        assertFalse(companyJobs.isEmpty());
        for(Jobs jobs: companyJobs){
            assertNotNull(jobs);
            assertNotNull(jobs.getId_job());
        }
    }
    @Test
    public void testCRUD() throws SQLException{
        Jobs jobs = createJobs();
        JobsDAO.addJobs(jobs);
        assertEquals(JobsDAO.getJobsById(jobs.getId_job()), "Software developer");
        JobsDAO.updateJobs(jobs);
        assertEquals(JobsDAO.getJobsById(jobs.getId_job()), "Java");
        JobsDAO.deleteJobs(jobs);
        assertNull(JobsDAO.getJobsById(jobs.getId_job()));
    }
}
