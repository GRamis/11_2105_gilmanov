import DAO.CompanyDAO;
import DAO.Factory;
import logic.Company;
import org.junit.Test;

import java.sql.SQLException;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Рамис on 14.03.14.
 */
public class CompanyDaoTest {
    private CompanyDAO CompanyDAO = Factory.getInstance().getCompanyDAO();

    private Company createCompany(){
        Company company = new Company();
        company.setName_company("Google");
        company.setTown("Mountain View");
        company.setEmail("google@google.com");
        company.setPassw("qwerty");
        return company;
    }

    @Test
    public void testFindAll() throws SQLException{
        List<Company> companyList = CompanyDAO.getAllCompany();
        assertNotNull(companyList);
        assertFalse(companyList.isEmpty());
        for(Company company: companyList){
            assertNotNull(company);
            assertNotNull(company.getId_company());
        }
    }
    @Test
    public void testCRUD() throws SQLException{
        Company company = createCompany();
        CompanyDAO.addCompany(company);
        assertEquals(CompanyDAO.getCompanyById(company.getId_company()).getJob(), "Software developer");
        CompanyDAO.updateCompany(company);
        assertEquals(CompanyDAO.getCompanyById(company.getId_company()).getJob(), "Java");
        CompanyDAO.deleteCompany(company);
        assertNull(CompanyDAO.getCompanyById(company.getId_company()));
    }
}
