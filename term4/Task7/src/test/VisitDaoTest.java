import DAO.Factory;
import DAO.VisitDAO;
import logic.Visit;
import org.junit.Test;

import java.sql.SQLException;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Рамис on 14.03.14.
 */
public class VisitDaoTest {

    private VisitDAO VisitDao = Factory.getInstance().getVisitDAO();

    private Visit createVisit() {
        Visit visit = new Visit();
        visit.setType("true");
        return visit;
    }

    @Test
    public void testFindAll() throws SQLException {
        List<Visit> VisitList = VisitDao.getAllVisit();
        assertNotNull(VisitList);
        assertFalse(VisitList.isEmpty());
        for(Visit visit: VisitList) {
            assertNotNull(visit);
            assertNotNull(visit.getId_resume());
        }
    }

    @Test
    public void testCRUD() throws SQLException {
        Visit visit = createVisit();
        VisitDao.addVisit(visit);
        assertEquals(VisitDao.getVisitById(visit.getId_job()).getType(), "Software developer");
        // Resume.getText("Programmer");
        VisitDao.updateVisit(visit);
        assertEquals(VisitDao.getVisitById(visit.getId_job()).getType(), "Programmer");
        VisitDao.deleteVisit(visit);
        assertNull(VisitDao.getVisitById(visit.getId_job()));
    }
}

