import DAO.CategoryDAO;
import DAO.Factory;
import logic.Category;
import org.junit.Test;

import java.sql.SQLException;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Рамис on 14.03.14.
 */
public class CategoryDaoTest {
    private CategoryDAO CategoryDao = Factory.getInstance().getCategoryDAO();

    private Category createCategory() {
        Category category = new Category();
        category.setName_categ("Technology");
        return category;
    }

    @Test
    public void testFindAll() throws SQLException {
        List<Category> CategoryList = CategoryDao.getAllCategory();
        assertNotNull(CategoryList);
        assertFalse(CategoryList.isEmpty());
        for(Category category: CategoryList) {
            assertNotNull(category);
            assertNotNull(category.getId_categ());
        }
    }

    @Test
    public void testCRUD() throws SQLException {
        Category category = createCategory();
        CategoryDao.addCategory(category);
        assertEquals(CategoryDao.getCategoryById(category.getId_categ()).getId_categ(), "Technology");
        CategoryDao.updateCategory(category);
        assertEquals(CategoryDao.getCategoryById(category.getId_categ()).getId_categ(), "Technology");
        CategoryDao.deleteCategory(category);
        assertNull(CategoryDao.getCategoryById(category.getId_categ()));
    }
}
