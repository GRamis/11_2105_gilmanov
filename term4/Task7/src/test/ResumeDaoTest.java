import DAO.*;
import logic.*;
import org.junit.Test;

import java.sql.SQLException;
import java.util.List;

import static org.junit.Assert.*;


public class ResumeDaoTest {

    private ResumeDAO ResumeDao = Factory.getInstance().getResumeDAO();

    private Resume createResume() {
        Resume resume = new Resume();
        Category category = new Category();
        category.setName_categ("Technology");
        Users user = new Users();
        user.setName_user("Ann");
        user.setTown("Kazan");
        user.setPhone("87654321");
        user.setEmail("ann@google.com");
        user.setPass("qwerty");
        resume.setGender("female");
        resume.setText("Python, Java, Ruby");
        resume.setEducation("Higher");
        resume.setExperience("5");
        resume.setGender("female");
        resume.setTitle("Software developer");
        Visit visit = new Visit();
        visit.setType("true");
        return resume;
    }

    @Test
    public void testFindAll() throws SQLException {
        List<Resume> ResumeList = ResumeDao.getAllResume();
        assertNotNull(ResumeList);
        assertFalse(ResumeList.isEmpty());
        for(Resume resume: ResumeList) {
            assertNotNull(resume);
            assertNotNull(resume.getId_resume());
        }
    }

    @Test
    public void testCRUD() throws SQLException {
        Resume resume = createResume();
        ResumeDao.addResume(resume);
        assertEquals(ResumeDao.getResumeById(resume.getId_resume()).getTitle(), "Software developer");
       // Resume.getText("Programmer");
        ResumeDao.updateResume(resume);
        assertEquals(ResumeDao.getResumeById(resume.getId_resume()).getTitle(), "Programmer");
        ResumeDao.deleteResume(resume);
        assertNull(ResumeDao.getResumeById(resume.getId_resume()));
    }
}
