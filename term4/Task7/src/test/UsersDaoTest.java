import DAO.Factory;
import DAO.UsersDAO;
import logic.Users;
import org.junit.Test;

import java.sql.SQLException;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Рамис on 14.03.14.
 */
public class UsersDaoTest {
    private UsersDAO UsersDao = Factory.getInstance().getUsersDAO();

    private Users createUsers() {
        Users user = new Users();
        user.setName_user("Ann");
        user.setTown("Kazan");
        user.setPhone("87654321");
        user.setEmail("ann@google.com");
        user.setPass("qwerty");
        return user;
    }

    @Test
    public void testFindAll() throws SQLException {
        List<Users> UsersList = UsersDao.getAllUsers();
        assertNotNull(UsersList);
        assertFalse(UsersList.isEmpty());
        for(Users users: UsersList) {
            assertNotNull(users);
            assertNotNull(users.getId_user());
        }
    }

    @Test
    public void testCRUD() throws SQLException {
        Users users = createUsers();
        UsersDao.addUsers(users);
        assertEquals(UsersDao.getUsersById(users.getId_user()), "Software developer");
        // Resume.getText("Programmer");
        UsersDao.updateUsers(users);
        assertEquals(UsersDao.getUsersById(users.getId_user()), "Programmer");
        UsersDao.deleteUsers(users);
        assertNull(UsersDao.getUsersById(users.getId_user()));
    }
}