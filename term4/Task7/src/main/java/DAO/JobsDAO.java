package DAO;

import logic.Jobs;
import logic.Users;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Рамис on 01.03.14.
 */
public interface JobsDAO{
    public void addJobs(Jobs jobs) throws SQLException;   //добавить работу
    public void updateJobs(Jobs jobs) throws SQLException;//обновить работу
    public Jobs getJobsById(Long id) throws SQLException;    //получить работу по id
    public List getAllJobs() throws SQLException;              //получить список работ
    public void deleteJobs(Jobs jobs) throws SQLException;//удалить работу
}
