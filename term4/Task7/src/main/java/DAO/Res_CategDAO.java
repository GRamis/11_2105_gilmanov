package DAO;

import logic.Res_Categ;
import logic.Users;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Рамис on 01.03.14.
 */
public interface Res_CategDAO{
    public void addRes_Categ(Res_Categ categ) throws SQLException;   //добавить категорию
    public void updateRes_Categ(Res_Categ categ) throws SQLException;//обновить категорию
    public Res_Categ getRes_CategById(Long id) throws SQLException;    //получить категорию по id
    public List getAllRes_Categ() throws SQLException;              //получить список категорий
    public void deleteRes_Categ(Res_Categ categ) throws SQLException;//удалить категорию
}
