package DAO;

import logic.Resume;
import logic.Users;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Рамис on 01.03.14.
 */
public interface ResumeDAO{
    public void addResume(Resume resume) throws SQLException;   //добавить резюме
    public void updateResume(Resume resume) throws SQLException;//обновить резюме
    public Resume getResumeById(int id) throws SQLException;    //получить резюме по id
    public List getAllResume() throws SQLException;              //получить список резюме
    public void deleteResume(Resume resume) throws SQLException;//удалить резюме
}
