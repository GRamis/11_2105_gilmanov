package logic;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * Created by Рамис on 26.02.14.
 */
@Entity
@Table(name="Category")
public class Category {

    int id_categ;
    String name_categ;

    @Id
    @GeneratedValue(generator="increment")
    @GenericGenerator(name="increment", strategy = "increment")
    @Column(name="id_categ")
    public int getId_categ() {
        return id_categ;
    }

    public void setId_categ(int id_categ) {
        this.id_categ = id_categ;
    }
    @Column(name="name_categ")
    public String getName_categ() {
        return name_categ;
    }

    public void setName_categ(String name_categ) {
        this.name_categ = name_categ;
    }
}

