package main;

import DAO.Factory;
import logic.*;

import java.sql.SQLException;
import java.util.List;

public class Main {
    
    public static void main(String[] args) throws SQLException {

        Users s1 = new Users();
        Users s2 = new Users();
        Category c1 = new Category();
        Resume r1 = new Resume();
        
        s1.setName_user("Ivanov Ivan");
        s1.setEmail("Ivanov@ya.ru");
        s1.setPass("3123");
        s1.setPhone("+4325");
        s1.setTown("Kazan");
        s2.setName_user("Petrov Petr");
        s2.setEmail("Petrov@ya.ru");
        s2.setPass("123");
        s2.setPhone("+123");
        s2.setTown("Kazan");

        Factory.getInstance().getUsersDAO().addUsers(s1);
        Factory.getInstance().getUsersDAO().addUsers(s2);

        c1.setName_categ("IT");
        Factory.getInstance().getCategoryDAO().addCategory(c1);

        r1.setId_user(s1);
        r1.setTitle("job");
        r1.setEducation("higher");
        r1.setExperience("1");
        r1.setGender("male");
        r1.setText("dsfsdfsd");

        //Вывод из бд
        List<Users> users = Factory.getInstance().getUsersDAO().getAllUsers();
        System.out.println("========Все пользователи=========");
        for(int i = 0; i < users.size(); ++i) {
                System.out.println("Имя пользователя : " + users.get(i).getName_user() + ", Телефон : " + users.get(i).getPhone() +",  id : " + users.get(i).getId_user());
                System.out.println("=============================");              
        }       
    }
}