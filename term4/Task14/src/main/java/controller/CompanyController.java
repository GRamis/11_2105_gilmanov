package controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import service.CompanyService;

/**
 * Created by Рамис on 29.03.14.
 */
@Controller
public class CompanyController {

    private CompanyService companyService;

    @Autowired
    public CompanyController(CompanyService companyService) {
        this.companyService = companyService;
    }

    @RequestMapping("/company/{id}")
    public ModelAndView getVacancyById(@PathVariable Long id) {
        return new ModelAndView("company_page", "company", companyService.getCompanyById(id));
    }
}
