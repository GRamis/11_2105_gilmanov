package com.springapp.mvc;

public class Student {
    private Integer phones;
    private String name;

    public void setPhones(Integer phones) {
        this.phones = phones;
    }
    public Integer getPhones() {
        return phones;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }

    public String toString(){
        return getName() +" " + getPhones();
    }
}