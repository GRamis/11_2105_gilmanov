<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Spring MVC Form Handling</title>
</head>
<body>

<h2>Submitted Student Information</h2>
<table>
    <tr>
        <td>Phones:</td>
        <td>${phones}</td>
        <c:if test="${list != null}">
        <c:forEach var="student" items="${list}">
    <tr>
        <td>Name</td>
        <td>${student.name}</td>
    </tr>
    <tr>
        <td>Mobile</td>
        <td>${student.phones}</td>
    </tr>
    </c:forEach>
    </c:if>
    </tr>
</table>
</body>
</html>