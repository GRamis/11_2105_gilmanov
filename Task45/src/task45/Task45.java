package task45;

/**
 *
 * @author Рамис
 */
import java.util.Random;
import java.util.concurrent.CountDownLatch;

public class Task45 {

    private static int c = 0;

    public static void main(String[] args) {

        final CountDownLatch start = new CountDownLatch(1);

        final int[] l = new int[50];
        for (int i = 0; i < 20; i++) {
            Thread thread = new Thread() {
                @Override
                public void run() {
                    try {
                        start.await();
                    } catch (InterruptedException e) {
                    }
                    Random r = new Random();
                    synchronized (l) {
                        while (c != 20) {
                            int i = r.nextInt(20);
                            if (l[i] != 1) {
                                l[i] = 1;
                                System.out.println("Number " + i);
                                c++;
                                break;
                            }
                        }
                    }
                }
            };
            thread.start();
        }
        start.countDown();
    }
}