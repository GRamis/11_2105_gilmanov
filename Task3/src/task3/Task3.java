package task3;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class Task3 {
	public static void main(String[] args) throws IOException {
			BufferedReader in = new BufferedReader(new FileReader("input.txt"));
			BufferedWriter out = new BufferedWriter(new FileWriter("output.txt"));
			Set<String> set = new HashSet <>();
			String a;
			while ( (a = in.readLine())!=null ){
				set.add(a + "\n");
			}
			for ( String n : set){
				out.write(n);
			}
                        if (in!= null){ 
				in.close();
			}
			if (out!=null){
				out.close();
			}
		}
		
	}