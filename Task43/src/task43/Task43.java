/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package task43;

/**
 *
 * @author �����
 */
import java.util.concurrent.CountDownLatch;

public class Task43 {
    public static void main (String[] args) throws InterruptedException {
        final CountDownLatch startGate = new CountDownLatch(1);
        final CountDownLatch finish = new CountDownLatch(1); 
        for (int i = 0; i < 5; i++) {
            Thread t = new Thread("Car #" + i) {
                @Override
                public void run() {
                    try {
                        startGate.await();
                        for (int i = 0; i < 10; i++){
                            System.out.printf("%s Lap %d%n", getName(), i);
                        }
                        finish.countDown();
                    } catch (InterruptedException e) {
                    }
                }
            };
            t.start();
        }
        startGate.countDown(); 
        finish.await();
        Long startTime = System.nanoTime();
        Long finishTime = System.nanoTime();
        System.out.println("������ ����� " + (finishTime-startTime));

    }
}